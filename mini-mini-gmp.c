#include <stddef.h>
#if defined (__cplusplus)
extern "C" {
#endif
typedef unsigned long mp_limb_t;
typedef long mp_size_t;
typedef unsigned long mp_bitcnt_t;
typedef mp_limb_t *mp_ptr;
typedef const mp_limb_t *mp_srcptr;
typedef struct
{
  int _mp_alloc;
  int _mp_size;
  mp_limb_t *_mp_d;
} __mpz_struct;
typedef __mpz_struct mpz_t[1];
typedef __mpz_struct *mpz_ptr;
typedef const __mpz_struct *mpz_srcptr;
extern const int mp_bits_per_limb;
#define mpn_invert_limb(x) mpn_invert_3by2 ((x), 0)
#define mpz_odd_p(z)   (((z)->_mp_size != 0) & (int) (z)->_mp_d[0])
#define mpz_even_p(z)  (! mpz_odd_p (z))
int mpz_cmp_ui (const mpz_t, unsigned long);
void mpz_mul_si (mpz_t, const mpz_t, long int);
void mpz_mul_ui (mpz_t, const mpz_t, unsigned long int);
int mpz_tstbit (const mpz_t, mp_bitcnt_t);
void mpz_setbit (mpz_t, mp_bitcnt_t);
void mpz_clrbit (mpz_t, mp_bitcnt_t);
void mpz_combit (mpz_t, mp_bitcnt_t);
mp_bitcnt_t mpz_scan1 (const mpz_t, mp_bitcnt_t);
#define MPZ_ROINIT_N(xp, xs) {{0, (xs),(xp) }}
void mpz_set_ui (mpz_t, unsigned long int);
size_t mpz_sizeinbase (const mpz_t, int);
#if defined (FILE)\
|| defined (H_STDIO)\
|| defined (_H_STDIO)\
|| defined (_STDIO_H)\
|| defined (_STDIO_H_)\
|| defined (__STDIO_H)\
|| defined (__STDIO_H__)\
|| defined (_STDIO_INCLUDED)\
|| defined (__dj_include_stdio_h_)\
|| defined (_FILE_DEFINED)\
|| defined (__STDIO__)\
|| defined (_MSL_STDIO_H)\
|| defined (_STDIO_H_INCLUDED)\
|| defined (_ISO_STDIO_ISO_H)\
|| defined (__STDIO_LOADED)         
size_t mpz_out_str (FILE *, int, const mpz_t);
#endif
#if defined (__cplusplus)
}
#endif
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#define GMP_LIMB_BITS (sizeof(mp_limb_t) * CHAR_BIT)
#define GMP_LIMB_MAX (~ (mp_limb_t) 0)
#define GMP_LIMB_HIGHBIT ((mp_limb_t) 1 << (GMP_LIMB_BITS - 1))
#define GMP_HLIMB_BIT ((mp_limb_t) 1 << (GMP_LIMB_BITS / 2))
#define GMP_LLIMB_MASK (GMP_HLIMB_BIT - 1)
#define GMP_ULONG_BITS (sizeof(unsigned long) * CHAR_BIT)
#define GMP_ULONG_HIGHBIT ((unsigned long) 1 << (GMP_ULONG_BITS - 1))
#define GMP_ABS(x) ((x) >= 0 ? (x) : -(x))
#define GMP_NEG_CAST(T,x) (-((T)((x) + 1) - 1))
#define GMP_MIN(a, b) ((a) < (b) ? (a) : (b))
#define GMP_MAX(a, b) ((a) > (b) ? (a) : (b))
#define gmp_assert_nocarry(x) do { \
    mp_limb_t __cy = (x);   \
    assert (__cy == 0);   \
  } while (0)
#define gmp_clz(count, x) do {\
    mp_limb_t __clz_x = (x);\
    unsigned __clz_c;\
    for (__clz_c = 0;\
 (__clz_x & ((mp_limb_t) 0xff << (GMP_LIMB_BITS - 8))) == 0;\
 __clz_c += 8)\
      __clz_x <<= 8;\
    for (; (__clz_x & GMP_LIMB_HIGHBIT) == 0; __clz_c++)\
      __clz_x <<= 1;\
    (count) = __clz_c;\
  } while (0)
#define gmp_ctz(count, x) do {\
    mp_limb_t __ctz_x = (x);\
    unsigned __ctz_c = 0;\
    gmp_clz (__ctz_c, __ctz_x & - __ctz_x);\
    (count) = GMP_LIMB_BITS - 1 - __ctz_c;\
  } while (0)
#define gmp_add_ssaaaa(sh, sl, ah, al, bh, bl) \
  do {\
    mp_limb_t __x;\
    __x = (al) + (bl);\
    (sh) = (ah) + (bh) + (__x < (al));\
    (sl) = __x;\
  } while (0)
#define gmp_sub_ddmmss(sh, sl, ah, al, bh, bl) \
  do {\
    mp_limb_t __x;\
    __x = (al) - (bl);\
    (sh) = (ah) - (bh) - ((al) < (bl));\
    (sl) = __x;\
  } while (0)
#define gmp_umul_ppmm(w1, w0, u, v)\
  do {\
    mp_limb_t __x0, __x1, __x2, __x3;\
    unsigned __ul, __vl, __uh, __vh;\
    mp_limb_t __u = (u), __v = (v);\
\
    __ul = __u & GMP_LLIMB_MASK;\
    __uh = __u >> (GMP_LIMB_BITS / 2);\
    __vl = __v & GMP_LLIMB_MASK;\
    __vh = __v >> (GMP_LIMB_BITS / 2);\
\
    __x0 = (mp_limb_t) __ul * __vl;\
    __x1 = (mp_limb_t) __ul * __vh;\
    __x2 = (mp_limb_t) __uh * __vl;\
    __x3 = (mp_limb_t) __uh * __vh;\
\
    __x1 += __x0 >> (GMP_LIMB_BITS / 2);\
    __x1 += __x2;\
    if (__x1 < __x2)\
      __x3 += GMP_HLIMB_BIT;\
\
    (w1) = __x3 + (__x1 >> (GMP_LIMB_BITS / 2));\
    (w0) = (__x1 << (GMP_LIMB_BITS / 2)) + (__x0 & GMP_LLIMB_MASK);\
  } while (0)
#define gmp_udiv_qrnnd_preinv(q, r, nh, nl, d, di)\
  do {\
    mp_limb_t _qh, _ql, _r, _mask;\
    gmp_umul_ppmm (_qh, _ql, (nh), (di));\
    gmp_add_ssaaaa (_qh, _ql, _qh, _ql, (nh) + 1, (nl));\
    _r = (nl) - _qh * (d);\
    _mask = -(mp_limb_t) (_r > _ql);\
    _qh += _mask;\
    _r += _mask & (d);\
    if (_r >= (d))\
      {\
_r -= (d);\
_qh++;\
      }\
\
    (r) = _r;\
    (q) = _qh;\
  } while (0)
#define gmp_udiv_qr_3by2(q, r1, r0, n2, n1, n0, d1, d0, dinv)\
  do {\
    mp_limb_t _q0, _t1, _t0, _mask;\
    gmp_umul_ppmm ((q), _q0, (n2), (dinv));\
    gmp_add_ssaaaa ((q), _q0, (q), _q0, (n2), (n1));\
    (r1) = (n1) - (d1) * (q);\
    gmp_sub_ddmmss ((r1), (r0), (r1), (n0), (d1), (d0));\
    gmp_umul_ppmm (_t1, _t0, (d0), (q));\
    gmp_sub_ddmmss ((r1), (r0), (r1), (r0), _t1, _t0);\
    (q)++;\
    _mask = - (mp_limb_t) ((r1) >= _q0);\
    (q) += _mask;\
    gmp_add_ssaaaa ((r1), (r0), (r1), (r0), _mask & (d1), _mask & (d0)); \
    if ((r1) >= (d1))\
      {\
if ((r1) > (d1) || (r0) >= (d0))\
  {\
    (q)++;\
    gmp_sub_ddmmss ((r1), (r0), (r1), (r0), (d1), (d0));\
  }\
      }\
  } while (0)
#define MP_LIMB_T_SWAP(x, y)\
  do {\
    mp_limb_t __mp_limb_t_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mp_limb_t_swap__tmp;\
  } while (0)
#define MP_SIZE_T_SWAP(x, y)\
  do {\
    mp_size_t __mp_size_t_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mp_size_t_swap__tmp;\
  } while (0)
#define MP_BITCNT_T_SWAP(x,y)\
  do {\
    mp_bitcnt_t __mp_bitcnt_t_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mp_bitcnt_t_swap__tmp;\
  } while (0)
#define MP_PTR_SWAP(x, y)\
  do {\
    mp_ptr __mp_ptr_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mp_ptr_swap__tmp;\
  } while (0)
#define MP_SRCPTR_SWAP(x, y)\
  do {\
    mp_srcptr __mp_srcptr_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mp_srcptr_swap__tmp;\
  } while (0)
#define MPN_PTR_SWAP(xp,xs, yp,ys)\
  do {\
    MP_PTR_SWAP (xp, yp);\
    MP_SIZE_T_SWAP (xs, ys);\
  } while(0)
#define MPN_SRCPTR_SWAP(xp,xs, yp,ys)\
  do {\
    MP_SRCPTR_SWAP (xp, yp);\
    MP_SIZE_T_SWAP (xs, ys);\
  } while(0)
#define MPZ_PTR_SWAP(x, y)\
  do {\
    mpz_ptr __mpz_ptr_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mpz_ptr_swap__tmp;\
  } while (0)
#define MPZ_SRCPTR_SWAP(x, y)\
  do {\
    mpz_srcptr __mpz_srcptr_swap__tmp = (x);\
    (x) = (y);\
    (y) = __mpz_srcptr_swap__tmp;\
  } while (0)
const int mp_bits_per_limb = GMP_LIMB_BITS;
static void
gmp_die (const char *msg)
{
  fprintf (stderr, "%s\n", msg);
  abort();
}
static void *
gmp_default_alloc (size_t size)
{
  void *p;
  assert (size > 0);
  p = malloc (size);
  if (!p)
    gmp_die("gmp_default_alloc: Virtual memory exhausted.");
  return p;
}
static void *
gmp_default_realloc (void *old, size_t old_size, size_t new_size)
{
  void * p;
  p = realloc (old, new_size);
  if (!p)
    gmp_die("gmp_default_realloc: Virtual memory exhausted.");
  return p;
}
static void
gmp_default_free (void *p, size_t size)
{
  free (p);
}
static void * (*gmp_allocate_func) (size_t) = gmp_default_alloc;
static void * (*gmp_reallocate_func) (void *, size_t, size_t) = gmp_default_realloc;
static void (*gmp_free_func) (void *, size_t) = gmp_default_free;
void
mp_get_memory_functions (void *(**alloc_func) (size_t),
 void *(**realloc_func) (void *, size_t, size_t),
 void (**free_func) (void *, size_t))
{
  if (alloc_func)
    *alloc_func = gmp_allocate_func;
  if (realloc_func)
    *realloc_func = gmp_reallocate_func;
  if (free_func)
    *free_func = gmp_free_func;
}
void
mp_set_memory_functions (void *(*alloc_func) (size_t),
 void *(*realloc_func) (void *, size_t, size_t),
 void (*free_func) (void *, size_t))
{
  if (!alloc_func)
    alloc_func = gmp_default_alloc;
  if (!realloc_func)
    realloc_func = gmp_default_realloc;
  if (!free_func)
    free_func = gmp_default_free;

  gmp_allocate_func = alloc_func;
  gmp_reallocate_func = realloc_func;
  gmp_free_func = free_func;
}
#define gmp_xalloc(size) ((*gmp_allocate_func)((size)))
#define gmp_free(p) ((*gmp_free_func) ((p), 0))
static mp_ptr
gmp_xalloc_limbs (mp_size_t size)
{
  return (mp_ptr) gmp_xalloc (size * sizeof (mp_limb_t));
}
static mp_ptr
gmp_xrealloc_limbs (mp_ptr old, mp_size_t size)
{
  assert (size > 0);
  return (mp_ptr) (*gmp_reallocate_func) (old, 0, size * sizeof (mp_limb_t));
}
void
mpn_copyi (mp_ptr d, mp_srcptr s, mp_size_t n)
{
  mp_size_t i;
  for (i = 0; i < n; i++)
    d[i] = s[i];
}
int
mpn_cmp (mp_srcptr ap, mp_srcptr bp, mp_size_t n)
{
  while (--n >= 0)
    {
      if (ap[n] != bp[n])
return ap[n] > bp[n] ? 1 : -1;
    }
  return 0;
}
static int
mpn_cmp4 (mp_srcptr ap, mp_size_t an, mp_srcptr bp, mp_size_t bn)
{
  if (an != bn)
    return an < bn ? -1 : 1;
  else
    return mpn_cmp (ap, bp, an);
}
static mp_size_t
mpn_normalized_size (mp_srcptr xp, mp_size_t n)
{
  while (n > 0 && xp[n-1] == 0)
    --n;
  return n;
}
int
mpn_zero_p(mp_srcptr rp, mp_size_t n)
{
  return mpn_normalized_size (rp, n) == 0;
}
mp_limb_t
mpn_add_1 (mp_ptr rp, mp_srcptr ap, mp_size_t n, mp_limb_t b)
{
  mp_size_t i;

  assert (n > 0);
  i = 0;
  do
    {
      mp_limb_t r = ap[i] + b;
      b = (r < b);
      rp[i] = r;
    }
  while (++i < n);

  return b;
}
mp_limb_t
mpn_add_n (mp_ptr rp, mp_srcptr ap, mp_srcptr bp, mp_size_t n)
{
  mp_size_t i;
  mp_limb_t cy;

  for (i = 0, cy = 0; i < n; i++)
    {
      mp_limb_t a, b, r;
      a = ap[i]; b = bp[i];
      r = a + cy;
      cy = (r < cy);
      r += b;
      cy += (r < b);
      rp[i] = r;
    }
  return cy;
}
mp_limb_t
mpn_add (mp_ptr rp, mp_srcptr ap, mp_size_t an, mp_srcptr bp, mp_size_t bn)
{
  mp_limb_t cy;

  assert (an >= bn);

  cy = mpn_add_n (rp, ap, bp, bn);
  if (an > bn)
    cy = mpn_add_1 (rp + bn, ap + bn, an - bn, cy);
  return cy;
}

mp_limb_t
mpn_sub_1 (mp_ptr rp, mp_srcptr ap, mp_size_t n, mp_limb_t b)
{
  mp_size_t i;

  assert (n > 0);

  i = 0;
  do
    {
      mp_limb_t a = ap[i];
      mp_limb_t cy = a < b;;
      rp[i] = a - b;
      b = cy;
    }
  while (++i < n);

  return b;
}

mp_limb_t
mpn_sub_n (mp_ptr rp, mp_srcptr ap, mp_srcptr bp, mp_size_t n)
{
  mp_size_t i;
  mp_limb_t cy;

  for (i = 0, cy = 0; i < n; i++)
    {
      mp_limb_t a, b;
      a = ap[i]; b = bp[i];
      b += cy;
      cy = (b < cy);
      cy += (a < b);
      rp[i] = a - b;
    }
  return cy;
}

mp_limb_t
mpn_sub (mp_ptr rp, mp_srcptr ap, mp_size_t an, mp_srcptr bp, mp_size_t bn)
{
  mp_limb_t cy;

  assert (an >= bn);

  cy = mpn_sub_n (rp, ap, bp, bn);
  if (an > bn)
    cy = mpn_sub_1 (rp + bn, ap + bn, an - bn, cy);
  return cy;
}

mp_limb_t
mpn_mul_1 (mp_ptr rp, mp_srcptr up, mp_size_t n, mp_limb_t vl)
{
  mp_limb_t ul, cl, hpl, lpl;

  assert (n >= 1);

  cl = 0;
  do
    {
      ul = *up++;
      gmp_umul_ppmm (hpl, lpl, ul, vl);

      lpl += cl;
      cl = (lpl < cl) + hpl;

      *rp++ = lpl;
    }
  while (--n != 0);

  return cl;
}

mp_limb_t
mpn_addmul_1 (mp_ptr rp, mp_srcptr up, mp_size_t n, mp_limb_t vl)
{
  mp_limb_t ul, cl, hpl, lpl, rl;

  assert (n >= 1);

  cl = 0;
  do
    {
      ul = *up++;
      gmp_umul_ppmm (hpl, lpl, ul, vl);

      lpl += cl;
      cl = (lpl < cl) + hpl;

      rl = *rp;
      lpl = rl + lpl;
      cl += lpl < rl;
      *rp++ = lpl;
    }
  while (--n != 0);

  return cl;
}

mp_limb_t
mpn_submul_1 (mp_ptr rp, mp_srcptr up, mp_size_t n, mp_limb_t vl)
{
  mp_limb_t ul, cl, hpl, lpl, rl;

  assert (n >= 1);

  cl = 0;
  do
    {
      ul = *up++;
      gmp_umul_ppmm (hpl, lpl, ul, vl);

      lpl += cl;
      cl = (lpl < cl) + hpl;

      rl = *rp;
      lpl = rl - lpl;
      cl += lpl > rl;
      *rp++ = lpl;
    }
  while (--n != 0);

  return cl;
}

mp_limb_t
mpn_mul (mp_ptr rp, mp_srcptr up, mp_size_t un, mp_srcptr vp, mp_size_t vn)
{
  assert (un >= vn);
  assert (vn >= 1);
  rp[un] = mpn_mul_1 (rp, up, un, vp[0]);

  while (--vn >= 1)
    {
      rp += 1, vp += 1;
      rp[un] = mpn_addmul_1 (rp, up, un, vp[0]);
    }
  return rp[un];
}

mp_limb_t
mpn_lshift (mp_ptr rp, mp_srcptr up, mp_size_t n, unsigned int cnt)
{
  mp_limb_t high_limb, low_limb;
  unsigned int tnc;
  mp_limb_t retval;

  assert (n >= 1);
  assert (cnt >= 1);
  assert (cnt < GMP_LIMB_BITS);

  up += n;
  rp += n;

  tnc = GMP_LIMB_BITS - cnt;
  low_limb = *--up;
  retval = low_limb >> tnc;
  high_limb = (low_limb << cnt);

  while (--n != 0)
    {
      low_limb = *--up;
      *--rp = high_limb | (low_limb >> tnc);
      high_limb = (low_limb << cnt);
    }
  *--rp = high_limb;

  return retval;
}

mp_limb_t
mpn_rshift (mp_ptr rp, mp_srcptr up, mp_size_t n, unsigned int cnt)
{
  mp_limb_t high_limb, low_limb;
  unsigned int tnc;
  mp_limb_t retval;

  assert (n >= 1);
  assert (cnt >= 1);
  assert (cnt < GMP_LIMB_BITS);

  tnc = GMP_LIMB_BITS - cnt;
  high_limb = *up++;
  retval = (high_limb << tnc);
  low_limb = high_limb >> cnt;

  while (--n != 0)
    {
      high_limb = *up++;
      *rp++ = low_limb | (high_limb << tnc);
      low_limb = high_limb >> cnt;
    }
  *rp = low_limb;

  return retval;
}

static mp_bitcnt_t
mpn_common_scan (mp_limb_t limb, mp_size_t i, mp_srcptr up, mp_size_t un,
 mp_limb_t ux)
{
  unsigned cnt;

  assert (ux == 0 || ux == GMP_LIMB_MAX);
  assert (0 <= i && i <= un );

  while (limb == 0)
    {
      i++;
      if (i == un)
return (ux == 0 ? ~(mp_bitcnt_t) 0 : un * GMP_LIMB_BITS);
      limb = ux ^ up[i];
    }
  gmp_ctz (cnt, limb);
  return (mp_bitcnt_t) i * GMP_LIMB_BITS + cnt;
}

mp_limb_t
mpn_invert_3by2 (mp_limb_t u1, mp_limb_t u0)
{
  mp_limb_t r, p, m;
  unsigned ul, uh;
  unsigned ql, qh;
  assert (u1 >= GMP_LIMB_HIGHBIT);

  ul = u1 & GMP_LLIMB_MASK;
  uh = u1 >> (GMP_LIMB_BITS / 2);

  qh = ~u1 / uh;
  r = ((~u1 - (mp_limb_t) qh * uh) << (GMP_LIMB_BITS / 2)) | GMP_LLIMB_MASK;

  p = (mp_limb_t) qh * ul;
  if (r < p)
    {
      qh--;
      r += u1;
      if (r >= u1)
if (r < p)
  {
    qh--;
    r += u1;
  }
    }
  r -= p;
  p = (r >> (GMP_LIMB_BITS / 2)) * qh + r;
  ql = (p >> (GMP_LIMB_BITS / 2)) + 1;
  r = (r << (GMP_LIMB_BITS / 2)) + GMP_LLIMB_MASK - ql * u1;

  if (r >= (p << (GMP_LIMB_BITS / 2)))
    {
      ql--;
      r += u1;
    }
  m = ((mp_limb_t) qh << (GMP_LIMB_BITS / 2)) + ql;
  if (r >= u1)
    {
      m++;
      r -= u1;
    }

  if (u0 > 0)
    {
      mp_limb_t th, tl;
      r = ~r;
      r += u0;
      if (r < u0)
{
  m--;
  if (r >= u1)
    {
      m--;
      r -= u1;
    }
  r -= u1;
}
      gmp_umul_ppmm (th, tl, u0, m);
      r += th;
      if (r < th)
{
  m--;
  m -= ((r > u1) | ((r == u1) & (tl > u0)));
}
    }

  return m;
}

struct gmp_div_inverse
{
  unsigned shift;
  mp_limb_t d1, d0;
  mp_limb_t di;
};

static void
mpn_div_qr_1_invert (struct gmp_div_inverse *inv, mp_limb_t d)
{
  unsigned shift;

  assert (d > 0);
  gmp_clz (shift, d);
  inv->shift = shift;
  inv->d1 = d << shift;
  inv->di = mpn_invert_limb (inv->d1);
}

static void
mpn_div_qr_2_invert (struct gmp_div_inverse *inv,
     mp_limb_t d1, mp_limb_t d0)
{
  unsigned shift;

  assert (d1 > 0);
  gmp_clz (shift, d1);
  inv->shift = shift;
  if (shift > 0)
    {
      d1 = (d1 << shift) | (d0 >> (GMP_LIMB_BITS - shift));
      d0 <<= shift;
    }
  inv->d1 = d1;
  inv->d0 = d0;
  inv->di = mpn_invert_3by2 (d1, d0);
}

static void
mpn_div_qr_invert (struct gmp_div_inverse *inv,
   mp_srcptr dp, mp_size_t dn)
{
  assert (dn > 0);

  if (dn == 1)
    mpn_div_qr_1_invert (inv, dp[0]);
  else if (dn == 2)
    mpn_div_qr_2_invert (inv, dp[1], dp[0]);
  else
    {
      unsigned shift;
      mp_limb_t d1, d0;

      d1 = dp[dn-1];
      d0 = dp[dn-2];
      assert (d1 > 0);
      gmp_clz (shift, d1);
      inv->shift = shift;
      if (shift > 0)
{
  d1 = (d1 << shift) | (d0 >> (GMP_LIMB_BITS - shift));
  d0 = (d0 << shift) | (dp[dn-3] >> (GMP_LIMB_BITS - shift));
}
      inv->d1 = d1;
      inv->d0 = d0;
      inv->di = mpn_invert_3by2 (d1, d0);
    }
}
static mp_limb_t
mpn_div_qr_1_preinv (mp_ptr qp, mp_srcptr np, mp_size_t nn,
     const struct gmp_div_inverse *inv)
{
  mp_limb_t d, di;
  mp_limb_t r;
  mp_ptr tp = NULL;

  if (inv->shift > 0)
    {
      tp = gmp_xalloc_limbs (nn);
      r = mpn_lshift (tp, np, nn, inv->shift);
      np = tp;
    }
  else
    r = 0;

  d = inv->d1;
  di = inv->di;
  while (--nn >= 0)
    {
      mp_limb_t q;

      gmp_udiv_qrnnd_preinv (q, r, r, np[nn], d, di);
      if (qp)
qp[nn] = q;
    }
  if (inv->shift > 0)
    gmp_free (tp);

  return r >> inv->shift;
}

static mp_limb_t
mpn_div_qr_1 (mp_ptr qp, mp_srcptr np, mp_size_t nn, mp_limb_t d)
{
  assert (d > 0);
  if ((d & (d-1)) == 0)
    {
      mp_limb_t r = np[0] & (d-1);
      if (qp)
{
  if (d <= 1)
    mpn_copyi (qp, np, nn);
  else
    {
      unsigned shift;
      gmp_ctz (shift, d);
      mpn_rshift (qp, np, nn, shift);
    }
}
      return r;
    }
  else
    {
      struct gmp_div_inverse inv;
      mpn_div_qr_1_invert (&inv, d);
      return mpn_div_qr_1_preinv (qp, np, nn, &inv);
    }
}

static void
mpn_div_qr_2_preinv (mp_ptr qp, mp_ptr rp, mp_srcptr np, mp_size_t nn,
     const struct gmp_div_inverse *inv)
{
  unsigned shift;
  mp_size_t i;
  mp_limb_t d1, d0, di, r1, r0;
  mp_ptr tp;

  assert (nn >= 2);
  shift = inv->shift;
  d1 = inv->d1;
  d0 = inv->d0;
  di = inv->di;

  if (shift > 0)
    {
      tp = gmp_xalloc_limbs (nn);
      r1 = mpn_lshift (tp, np, nn, shift);
      np = tp;
    }
  else
    r1 = 0;

  r0 = np[nn - 1];

  i = nn - 2;
  do
    {
      mp_limb_t n0, q;
      n0 = np[i];
      gmp_udiv_qr_3by2 (q, r1, r0, r1, r0, n0, d1, d0, di);

      if (qp)
qp[i] = q;
    }
  while (--i >= 0);

  if (shift > 0)
    {
      assert ((r0 << (GMP_LIMB_BITS - shift)) == 0);
      r0 = (r0 >> shift) | (r1 << (GMP_LIMB_BITS - shift));
      r1 >>= shift;

      gmp_free (tp);
    }

  rp[1] = r1;
  rp[0] = r0;
}
static void
mpn_div_qr_pi1 (mp_ptr qp,
mp_ptr np, mp_size_t nn, mp_limb_t n1,
mp_srcptr dp, mp_size_t dn,
mp_limb_t dinv)
{
  mp_size_t i;

  mp_limb_t d1, d0;
  mp_limb_t cy, cy1;
  mp_limb_t q;

  assert (dn > 2);
  assert (nn >= dn);

  d1 = dp[dn - 1];
  d0 = dp[dn - 2];

  assert ((d1 & GMP_LIMB_HIGHBIT) != 0);

  i = nn - dn;
  do
    {
      mp_limb_t n0 = np[dn-1+i];

      if (n1 == d1 && n0 == d0)
{
  q = GMP_LIMB_MAX;
  mpn_submul_1 (np+i, dp, dn, q);
  n1 = np[dn-1+i];
}
      else
{
  gmp_udiv_qr_3by2 (q, n1, n0, n1, n0, np[dn-2+i], d1, d0, dinv);

  cy = mpn_submul_1 (np + i, dp, dn-2, q);

  cy1 = n0 < cy;
  n0 = n0 - cy;
  cy = n1 < cy1;
  n1 = n1 - cy1;
  np[dn-2+i] = n0;

  if (cy != 0)
    {
      n1 += d1 + mpn_add_n (np + i, np + i, dp, dn - 1);
      q--;
    }
}

      if (qp)
qp[i] = q;
    }
  while (--i >= 0);

  np[dn - 1] = n1;
}

static void
mpn_div_qr_preinv (mp_ptr qp, mp_ptr np, mp_size_t nn,
   mp_srcptr dp, mp_size_t dn,
   const struct gmp_div_inverse *inv)
{
  assert (dn > 0);
  assert (nn >= dn);

  if (dn == 1)
    np[0] = mpn_div_qr_1_preinv (qp, np, nn, inv);
  else if (dn == 2)
    mpn_div_qr_2_preinv (qp, np, np, nn, inv);
  else
    {
      mp_limb_t nh;
      unsigned shift;

      assert (inv->d1 == dp[dn-1]);
      assert (inv->d0 == dp[dn-2]);
      assert ((inv->d1 & GMP_LIMB_HIGHBIT) != 0);

      shift = inv->shift;
      if (shift > 0)
nh = mpn_lshift (np, np, nn, shift);
      else
nh = 0;

      mpn_div_qr_pi1 (qp, np, nn, nh, dp, dn, inv->di);

      if (shift > 0)
gmp_assert_nocarry (mpn_rshift (np, np, dn, shift));
    }
}

static void
mpn_div_qr (mp_ptr qp, mp_ptr np, mp_size_t nn, mp_srcptr dp, mp_size_t dn)
{
  struct gmp_div_inverse inv;
  mp_ptr tp = NULL;

  assert (dn > 0);
  assert (nn >= dn);

  mpn_div_qr_invert (&inv, dp, dn);
  if (dn > 2 && inv.shift > 0)
    {
      tp = gmp_xalloc_limbs (dn);
      gmp_assert_nocarry (mpn_lshift (tp, dp, dn, inv.shift));
      dp = tp;
    }
  mpn_div_qr_preinv (qp, np, nn, dp, dn, &inv);
  if (tp)
    gmp_free (tp);
}

static unsigned
mpn_base_power_of_two_p (unsigned b)
{
  switch (b)
    {
    case 2: return 1;
    case 4: return 2;
    case 8: return 3;
    case 16: return 4;
    case 32: return 5;
    case 64: return 6;
    case 128: return 7;
    case 256: return 8;
    default: return 0;
    }
}

struct mpn_base_info
{
  unsigned exp;
  mp_limb_t bb;
};

static void
mpn_get_base_info (struct mpn_base_info *info, mp_limb_t b)
{
  mp_limb_t m;
  mp_limb_t p;
  unsigned exp;

  m = GMP_LIMB_MAX / b;
  for (exp = 1, p = b; p <= m; exp++)
    p *= b;

  info->exp = exp;
  info->bb = p;
}

static mp_bitcnt_t
mpn_limb_size_in_base_2 (mp_limb_t u)
{
  unsigned shift;

  assert (u > 0);
  gmp_clz (shift, u);
  return GMP_LIMB_BITS - shift;
}

static size_t
mpn_get_str_bits (unsigned char *sp, unsigned bits, mp_srcptr up, mp_size_t un)
{
  unsigned char mask;
  size_t sn, j;
  mp_size_t i;
  int shift;

  sn = ((un - 1) * GMP_LIMB_BITS + mpn_limb_size_in_base_2 (up[un-1])
+ bits - 1) / bits;

  mask = (1U << bits) - 1;

  for (i = 0, j = sn, shift = 0; j-- > 0;)
    {
      unsigned char digit = up[i] >> shift;

      shift += bits;

      if (shift >= GMP_LIMB_BITS && ++i < un)
{
  shift -= GMP_LIMB_BITS;
  digit |= up[i] << (bits - shift);
}
      sp[j] = digit & mask;
    }
  return sn;
}
static size_t
mpn_limb_get_str (unsigned char *sp, mp_limb_t w,
  const struct gmp_div_inverse *binv)
{
  mp_size_t i;
  for (i = 0; w > 0; i++)
    {
      mp_limb_t h, l, r;

      h = w >> (GMP_LIMB_BITS - binv->shift);
      l = w << binv->shift;

      gmp_udiv_qrnnd_preinv (w, r, h, l, binv->d1, binv->di);
      assert ( (r << (GMP_LIMB_BITS - binv->shift)) == 0);
      r >>= binv->shift;

      sp[i] = r;
    }
  return i;
}

static size_t
mpn_get_str_other (unsigned char *sp,
   int base, const struct mpn_base_info *info,
   mp_ptr up, mp_size_t un)
{
  struct gmp_div_inverse binv;
  size_t sn;
  size_t i;

  mpn_div_qr_1_invert (&binv, base);

  sn = 0;

  if (un > 1)
    {
      struct gmp_div_inverse bbinv;
      mpn_div_qr_1_invert (&bbinv, info->bb);

      do
{
  mp_limb_t w;
  size_t done;
  w = mpn_div_qr_1_preinv (up, up, un, &bbinv);
  un -= (up[un-1] == 0);
  done = mpn_limb_get_str (sp + sn, w, &binv);

  for (sn += done; done < info->exp; done++)
    sp[sn++] = 0;
}
      while (un > 1);
    }
  sn += mpn_limb_get_str (sp + sn, up[0], &binv);
  for (i = 0; 2*i + 1 < sn; i++)
    {
      unsigned char t = sp[i];
      sp[i] = sp[sn - i - 1];
      sp[sn - i - 1] = t;
    }

  return sn;
}

size_t
mpn_get_str (unsigned char *sp, int base, mp_ptr up, mp_size_t un)
{
  unsigned bits;

  assert (un > 0);
  assert (up[un-1] > 0);

  bits = mpn_base_power_of_two_p (base);
  if (bits)
    return mpn_get_str_bits (sp, bits, up, un);
  else
    {
      struct mpn_base_info info;

      mpn_get_base_info (&info, base);
      return mpn_get_str_other (sp, base, &info, up, un);
    }
}

static mp_size_t
mpn_set_str_bits (mp_ptr rp, const unsigned char *sp, size_t sn,
  unsigned bits)
{
  mp_size_t rn;
  size_t j;
  unsigned shift;

  for (j = sn, rn = 0, shift = 0; j-- > 0; )
    {
      if (shift == 0)
{
  rp[rn++] = sp[j];
  shift += bits;
}
      else
{
  rp[rn-1] |= (mp_limb_t) sp[j] << shift;
  shift += bits;
  if (shift >= GMP_LIMB_BITS)
    {
      shift -= GMP_LIMB_BITS;
      if (shift > 0)
rp[rn++] = (mp_limb_t) sp[j] >> (bits - shift);
    }
}
    }
  rn = mpn_normalized_size (rp, rn);
  return rn;
}

static mp_size_t
mpn_set_str_other (mp_ptr rp, const unsigned char *sp, size_t sn,
   mp_limb_t b, const struct mpn_base_info *info)
{
  mp_size_t rn;
  mp_limb_t w;
  unsigned k;
  size_t j;

  k = 1 + (sn - 1) % info->exp;

  j = 0;
  w = sp[j++];
  while (--k != 0)
    w = w * b + sp[j++];

  rp[0] = w;

  for (rn = (w > 0); j < sn;)
    {
      mp_limb_t cy;

      w = sp[j++];
      for (k = 1; k < info->exp; k++)
w = w * b + sp[j++];

      cy = mpn_mul_1 (rp, rp, rn, info->bb);
      cy += mpn_add_1 (rp, rp, rn, w);
      if (cy > 0)
rp[rn++] = cy;
    }
  assert (j == sn);

  return rn;
}

mp_size_t
mpn_set_str (mp_ptr rp, const unsigned char *sp, size_t sn, int base)
{
  unsigned bits;

  if (sn == 0)
    return 0;

  bits = mpn_base_power_of_two_p (base);
  if (bits)
    return mpn_set_str_bits (rp, sp, sn, bits);
  else
    {
      struct mpn_base_info info;

      mpn_get_base_info (&info, base);
      return mpn_set_str_other (rp, sp, sn, base, &info);
    }
}

void
mpz_init (mpz_t r)
{
  r->_mp_alloc = 1;
  r->_mp_size = 0;
  r->_mp_d = gmp_xalloc_limbs (1);
}
void
mpz_init2 (mpz_t r, mp_bitcnt_t bits)
{
  mp_size_t rn;

  bits -= (bits != 0);
  rn = 1 + bits / GMP_LIMB_BITS;

  r->_mp_alloc = rn;
  r->_mp_size = 0;
  r->_mp_d = gmp_xalloc_limbs (rn);
}

void
mpz_clear (mpz_t r)
{
  gmp_free (r->_mp_d);
}

static mp_ptr
mpz_realloc (mpz_t r, mp_size_t size)
{
  size = GMP_MAX (size, 1);

  r->_mp_d = gmp_xrealloc_limbs (r->_mp_d, size);
  r->_mp_alloc = size;

  if (GMP_ABS (r->_mp_size) > size)
    r->_mp_size = 0;

  return r->_mp_d;
}
#define MPZ_REALLOC(z,n) ((n) > (z)->_mp_alloc\
  ? mpz_realloc(z,n)\
  : (z)->_mp_d)
void
mpz_set_si (mpz_t r, signed long int x)
{
  if (x >= 0)
    mpz_set_ui (r, x);
  else
    {
      r->_mp_size = -1;
      r->_mp_d[0] = GMP_NEG_CAST (unsigned long int, x);
    }
}

void
mpz_set_ui (mpz_t r, unsigned long int x)
{
  if (x > 0)
    {
      r->_mp_size = 1;
      r->_mp_d[0] = x;
    }
  else
    r->_mp_size = 0;
}

void
mpz_set (mpz_t r, const mpz_t x)
{
  if (r != x)
    {
      mp_size_t n;
      mp_ptr rp;

      n = GMP_ABS (x->_mp_size);
      rp = MPZ_REALLOC (r, n);

      mpn_copyi (rp, x->_mp_d, n);
      r->_mp_size = x->_mp_size;
    }
}

void
mpz_init_set_si (mpz_t r, signed long int x)
{
  mpz_init (r);
  mpz_set_si (r, x);
}

void
mpz_init_set_ui (mpz_t r, unsigned long int x)
{
  mpz_init (r);
  mpz_set_ui (r, x);
}

void
mpz_init_set (mpz_t r, const mpz_t x)
{
  mpz_init (r);
  mpz_set (r, x);
}

unsigned long int
mpz_get_ui (const mpz_t u)
{
  return u->_mp_size == 0 ? 0 : u->_mp_d[0];
}

int
mpz_sgn (const mpz_t u)
{
  mp_size_t usize = u->_mp_size;

  return (usize > 0) - (usize < 0);
}

int
mpz_cmp_si (const mpz_t u, long v)
{
  mp_size_t usize = u->_mp_size;

  if (usize < -1)
    return -1;
  else if (v >= 0)
    return mpz_cmp_ui (u, v);
  else if (usize >= 0)
    return 1;
  else
    {
      mp_limb_t ul = u->_mp_d[0];
      if ((mp_limb_t)GMP_NEG_CAST (unsigned long int, v) < ul)
return -1;
      else
return (mp_limb_t)GMP_NEG_CAST (unsigned long int, v) > ul;
    }
}

int
mpz_cmp_ui (const mpz_t u, unsigned long v)
{
  mp_size_t usize = u->_mp_size;

  if (usize > 1)
    return 1;
  else if (usize < 0)
    return -1;
  else
    {
      mp_limb_t ul = (usize > 0) ? u->_mp_d[0] : 0;
      return (ul > v) - (ul < v);
    }
}

int
mpz_cmp (const mpz_t a, const mpz_t b)
{
  mp_size_t asize = a->_mp_size;
  mp_size_t bsize = b->_mp_size;

  if (asize != bsize)
    return (asize < bsize) ? -1 : 1;
  else if (asize >= 0)
    return mpn_cmp (a->_mp_d, b->_mp_d, asize);
  else
    return mpn_cmp (b->_mp_d, a->_mp_d, -asize);
}

void
mpz_abs (mpz_t r, const mpz_t u)
{
  mpz_set (r, u);
  r->_mp_size = GMP_ABS (r->_mp_size);
}

void
mpz_neg (mpz_t r, const mpz_t u)
{
  mpz_set (r, u);
  r->_mp_size = -r->_mp_size;
}

void
mpz_swap (mpz_t u, mpz_t v)
{
  MP_SIZE_T_SWAP (u->_mp_size, v->_mp_size);
  MP_SIZE_T_SWAP (u->_mp_alloc, v->_mp_alloc);
  MP_PTR_SWAP (u->_mp_d, v->_mp_d);
}
static mp_size_t
mpz_abs_add_ui (mpz_t r, const mpz_t a, unsigned long b)
{
  mp_size_t an;
  mp_ptr rp;
  mp_limb_t cy;

  an = GMP_ABS (a->_mp_size);
  if (an == 0)
    {
      r->_mp_d[0] = b;
      return b > 0;
    }

  rp = MPZ_REALLOC (r, an + 1);

  cy = mpn_add_1 (rp, a->_mp_d, an, b);
  rp[an] = cy;
  an += cy;

  return an;
}
static mp_size_t
mpz_abs_sub_ui (mpz_t r, const mpz_t a, unsigned long b)
{
  mp_size_t an = GMP_ABS (a->_mp_size);
  mp_ptr rp = MPZ_REALLOC (r, an);

  if (an == 0)
    {
      rp[0] = b;
      return -(b > 0);
    }
  else if (an == 1 && a->_mp_d[0] < b)
    {
      rp[0] = b - a->_mp_d[0];
      return -1;
    }
  else
    {
      gmp_assert_nocarry (mpn_sub_1 (rp, a->_mp_d, an, b));
      return mpn_normalized_size (rp, an);
    }
}

void
mpz_add_ui (mpz_t r, const mpz_t a, unsigned long b)
{
  if (a->_mp_size >= 0)
    r->_mp_size = mpz_abs_add_ui (r, a, b);
  else
    r->_mp_size = -mpz_abs_sub_ui (r, a, b);
}

void
mpz_sub_ui (mpz_t r, const mpz_t a, unsigned long b)
{
  if (a->_mp_size < 0)
    r->_mp_size = -mpz_abs_add_ui (r, a, b);
  else
    r->_mp_size = mpz_abs_sub_ui (r, a, b);
}

void
mpz_ui_sub (mpz_t r, unsigned long a, const mpz_t b)
{
  if (b->_mp_size < 0)
    r->_mp_size = mpz_abs_add_ui (r, b, a);
  else
    r->_mp_size = -mpz_abs_sub_ui (r, b, a);
}

static mp_size_t
mpz_abs_add (mpz_t r, const mpz_t a, const mpz_t b)
{
  mp_size_t an = GMP_ABS (a->_mp_size);
  mp_size_t bn = GMP_ABS (b->_mp_size);
  mp_ptr rp;
  mp_limb_t cy;

  if (an < bn)
    {
      MPZ_SRCPTR_SWAP (a, b);
      MP_SIZE_T_SWAP (an, bn);
    }

  rp = MPZ_REALLOC (r, an + 1);
  cy = mpn_add (rp, a->_mp_d, an, b->_mp_d, bn);

  rp[an] = cy;

  return an + cy;
}

static mp_size_t
mpz_abs_sub (mpz_t r, const mpz_t a, const mpz_t b)
{
  mp_size_t an = GMP_ABS (a->_mp_size);
  mp_size_t bn = GMP_ABS (b->_mp_size);
  int cmp;
  mp_ptr rp;

  cmp = mpn_cmp4 (a->_mp_d, an, b->_mp_d, bn);
  if (cmp > 0)
    {
      rp = MPZ_REALLOC (r, an);
      gmp_assert_nocarry (mpn_sub (rp, a->_mp_d, an, b->_mp_d, bn));
      return mpn_normalized_size (rp, an);
    }
  else if (cmp < 0)
    {
      rp = MPZ_REALLOC (r, bn);
      gmp_assert_nocarry (mpn_sub (rp, b->_mp_d, bn, a->_mp_d, an));
      return -mpn_normalized_size (rp, bn);
    }
  else
    return 0;
}

void
mpz_add (mpz_t r, const mpz_t a, const mpz_t b)
{
  mp_size_t rn;

  if ( (a->_mp_size ^ b->_mp_size) >= 0)
    rn = mpz_abs_add (r, a, b);
  else
    rn = mpz_abs_sub (r, a, b);

  r->_mp_size = a->_mp_size >= 0 ? rn : - rn;
}

void
mpz_sub (mpz_t r, const mpz_t a, const mpz_t b)
{
  mp_size_t rn;

  if ( (a->_mp_size ^ b->_mp_size) >= 0)
    rn = mpz_abs_sub (r, a, b);
  else
    rn = mpz_abs_add (r, a, b);

  r->_mp_size = a->_mp_size >= 0 ? rn : - rn;
}

void
mpz_mul_si (mpz_t r, const mpz_t u, long int v)
{
  if (v < 0)
    {
      mpz_mul_ui (r, u, GMP_NEG_CAST (unsigned long int, v));
      mpz_neg (r, r);
    }
  else
    mpz_mul_ui (r, u, (unsigned long int) v);
}

void
mpz_mul_ui (mpz_t r, const mpz_t u, unsigned long int v)
{
  mp_size_t un, us;
  mp_ptr tp;
  mp_limb_t cy;

  us = u->_mp_size;

  if (us == 0 || v == 0)
    {
      r->_mp_size = 0;
      return;
    }

  un = GMP_ABS (us);

  tp = MPZ_REALLOC (r, un + 1);
  cy = mpn_mul_1 (tp, u->_mp_d, un, v);
  tp[un] = cy;

  un += (cy > 0);
  r->_mp_size = (us < 0) ? - un : un;
}

void
mpz_mul (mpz_t r, const mpz_t u, const mpz_t v)
{
  int sign;
  mp_size_t un, vn, rn;
  mpz_t t;
  mp_ptr tp;

  un = u->_mp_size;
  vn = v->_mp_size;

  if (un == 0 || vn == 0)
    {
      r->_mp_size = 0;
      return;
    }

  sign = (un ^ vn) < 0;

  un = GMP_ABS (un);
  vn = GMP_ABS (vn);

  mpz_init2 (t, (un + vn) * GMP_LIMB_BITS);

  tp = t->_mp_d;
  if (un >= vn)
    mpn_mul (tp, u->_mp_d, un, v->_mp_d, vn);
  else
    mpn_mul (tp, v->_mp_d, vn, u->_mp_d, un);

  rn = un + vn;
  rn -= tp[rn-1] == 0;

  t->_mp_size = sign ? - rn : rn;
  mpz_swap (r, t);
  mpz_clear (t);
}

enum mpz_div_round_mode { GMP_DIV_FLOOR, GMP_DIV_CEIL, GMP_DIV_TRUNC };
static int
mpz_div_qr (mpz_t q, mpz_t r,
    const mpz_t n, const mpz_t d, enum mpz_div_round_mode mode)
{
  mp_size_t ns, ds, nn, dn, qs;
  ns = n->_mp_size;
  ds = d->_mp_size;

  if (ds == 0)
    gmp_die("mpz_div_qr: Divide by zero.");

  if (ns == 0)
    {
      if (q)
q->_mp_size = 0;
      if (r)
r->_mp_size = 0;
      return 0;
    }

  nn = GMP_ABS (ns);
  dn = GMP_ABS (ds);

  qs = ds ^ ns;

  if (nn < dn)
    {
      if (mode == GMP_DIV_CEIL && qs >= 0)
{
  if (r)
    mpz_sub (r, n, d);
  if (q)
    mpz_set_ui (q, 1);
}
      else if (mode == GMP_DIV_FLOOR && qs < 0)
{
  if (r)
    mpz_add (r, n, d);
  if (q)
    mpz_set_si (q, -1);
}
      else
{
  if (r)
    mpz_set (r, n);
  if (q)
    q->_mp_size = 0;
}
      return 1;
    }
  else
    {
      mp_ptr np, qp;
      mp_size_t qn, rn;
      mpz_t tq, tr;

      mpz_init_set (tr, n);
      np = tr->_mp_d;

      qn = nn - dn + 1;

      if (q)
{
  mpz_init2 (tq, qn * GMP_LIMB_BITS);
  qp = tq->_mp_d;
}
      else
qp = NULL;

      mpn_div_qr (qp, np, nn, d->_mp_d, dn);

      if (qp)
{
  qn -= (qp[qn-1] == 0);

  tq->_mp_size = qs < 0 ? -qn : qn;
}
      rn = mpn_normalized_size (np, dn);
      tr->_mp_size = ns < 0 ? - rn : rn;

      if (mode == GMP_DIV_FLOOR && qs < 0 && rn != 0)
{
  if (q)
    mpz_sub_ui (tq, tq, 1);
  if (r)
    mpz_add (tr, tr, d);
}
      else if (mode == GMP_DIV_CEIL && qs >= 0 && rn != 0)
{
  if (q)
    mpz_add_ui (tq, tq, 1);
  if (r)
    mpz_sub (tr, tr, d);
}

      if (q)
{
  mpz_swap (tq, q);
  mpz_clear (tq);
}
      if (r)
mpz_swap (tr, r);

      mpz_clear (tr);

      return rn != 0;
    }
}

void
mpz_mod (mpz_t r, const mpz_t n, const mpz_t d)
{
  mpz_div_qr (NULL, r, n, d, d->_mp_size >= 0 ? GMP_DIV_FLOOR : GMP_DIV_CEIL);
}

void
mpz_divexact (mpz_t q, const mpz_t n, const mpz_t d)
{
  gmp_assert_nocarry (mpz_div_qr (q, NULL, n, d, GMP_DIV_TRUNC));
}

static unsigned long
mpz_div_qr_ui (mpz_t q, mpz_t r,
       const mpz_t n, unsigned long d, enum mpz_div_round_mode mode)
{
  mp_size_t ns, qn;
  mp_ptr qp;
  mp_limb_t rl;
  mp_size_t rs;

  ns = n->_mp_size;
  if (ns == 0)
    {
      if (q)
q->_mp_size = 0;
      if (r)
r->_mp_size = 0;
      return 0;
    }

  qn = GMP_ABS (ns);
  if (q)
    qp = MPZ_REALLOC (q, qn);
  else
    qp = NULL;

  rl = mpn_div_qr_1 (qp, n->_mp_d, qn, d);
  assert (rl < d);

  rs = rl > 0;
  rs = (ns < 0) ? -rs : rs;

  if (rl > 0 && ( (mode == GMP_DIV_FLOOR && ns < 0)
  || (mode == GMP_DIV_CEIL && ns >= 0)))
    {
      if (q)
gmp_assert_nocarry (mpn_add_1 (qp, qp, qn, 1));
      rl = d - rl;
      rs = -rs;
    }

  if (r)
    {
      r->_mp_d[0] = rl;
      r->_mp_size = rs;
    }
  if (q)
    {
      qn -= (qp[qn-1] == 0);
      assert (qn == 0 || qp[qn-1] > 0);

      q->_mp_size = (ns < 0) ? - qn : qn;
    }

  return rl;
}

unsigned long
mpz_fdiv_q_ui (mpz_t q, const mpz_t n, unsigned long d)
{
  return mpz_div_qr_ui (q, NULL, n, d, GMP_DIV_FLOOR);
}

unsigned long
mpz_mod_ui (mpz_t r, const mpz_t n, unsigned long d)
{
  return mpz_div_qr_ui (NULL, r, n, d, GMP_DIV_FLOOR);
}

void
mpz_divexact_ui (mpz_t q, const mpz_t n, unsigned long d)
{
  gmp_assert_nocarry (mpz_div_qr_ui (q, NULL, n, d, GMP_DIV_TRUNC));
}

int
mpz_tstbit (const mpz_t d, mp_bitcnt_t bit_index)
{
  mp_size_t limb_index;
  unsigned shift;
  mp_size_t ds;
  mp_size_t dn;
  mp_limb_t w;
  int bit;

  ds = d->_mp_size;
  dn = GMP_ABS (ds);
  limb_index = bit_index / GMP_LIMB_BITS;
  if (limb_index >= dn)
    return ds < 0;

  shift = bit_index % GMP_LIMB_BITS;
  w = d->_mp_d[limb_index];
  bit = (w >> shift) & 1;

  if (ds < 0)
    {
      if (shift > 0 && (w << (GMP_LIMB_BITS - shift)) > 0)
return bit ^ 1;
      while (--limb_index >= 0)
if (d->_mp_d[limb_index] > 0)
  return bit ^ 1;
    }
  return bit;
}

static void
mpz_abs_add_bit (mpz_t d, mp_bitcnt_t bit_index)
{
  mp_size_t dn, limb_index;
  mp_limb_t bit;
  mp_ptr dp;

  dn = GMP_ABS (d->_mp_size);

  limb_index = bit_index / GMP_LIMB_BITS;
  bit = (mp_limb_t) 1 << (bit_index % GMP_LIMB_BITS);

  if (limb_index >= dn)
    {
      mp_size_t i;
      dp = MPZ_REALLOC (d, limb_index + 1);

      dp[limb_index] = bit;
      for (i = dn; i < limb_index; i++)
dp[i] = 0;
      dn = limb_index + 1;
    }
  else
    {
      mp_limb_t cy;

      dp = d->_mp_d;

      cy = mpn_add_1 (dp + limb_index, dp + limb_index, dn - limb_index, bit);
      if (cy > 0)
{
  dp = MPZ_REALLOC (d, dn + 1);
  dp[dn++] = cy;
}
    }

  d->_mp_size = (d->_mp_size < 0) ? - dn : dn;
}

static void
mpz_abs_sub_bit (mpz_t d, mp_bitcnt_t bit_index)
{
  mp_size_t dn, limb_index;
  mp_ptr dp;
  mp_limb_t bit;

  dn = GMP_ABS (d->_mp_size);
  dp = d->_mp_d;

  limb_index = bit_index / GMP_LIMB_BITS;
  bit = (mp_limb_t) 1 << (bit_index % GMP_LIMB_BITS);

  assert (limb_index < dn);

  gmp_assert_nocarry (mpn_sub_1 (dp + limb_index, dp + limb_index,
 dn - limb_index, bit));
  dn = mpn_normalized_size (dp, dn);
  d->_mp_size = (d->_mp_size < 0) ? - dn : dn;
}

void
mpz_setbit (mpz_t d, mp_bitcnt_t bit_index)
{
  if (!mpz_tstbit (d, bit_index))
    {
      if (d->_mp_size >= 0)
mpz_abs_add_bit (d, bit_index);
      else
mpz_abs_sub_bit (d, bit_index);
    }
}

mp_bitcnt_t
mpz_scan1 (const mpz_t u, mp_bitcnt_t starting_bit)
{
  mp_ptr up;
  mp_size_t us, un, i;
  mp_limb_t limb, ux;

  us = u->_mp_size;
  un = GMP_ABS (us);
  i = starting_bit / GMP_LIMB_BITS;
  if (i >= un)
    return (us >= 0 ? ~(mp_bitcnt_t) 0 : starting_bit);

  up = u->_mp_d;
  ux = 0;
  limb = up[i];

  if (starting_bit != 0)
    {
      if (us < 0)
{
  ux = mpn_zero_p (up, i);
  limb = ~ limb + ux;
  ux = - (mp_limb_t) (limb >= ux);
}
      limb &= (GMP_LIMB_MAX << (starting_bit % GMP_LIMB_BITS));
    }

  return mpn_common_scan (limb, i, up, un, ux);
}

size_t
mpz_sizeinbase (const mpz_t u, int base)
{
  mp_size_t un;
  mp_srcptr up;
  mp_ptr tp;
  mp_bitcnt_t bits;
  struct gmp_div_inverse bi;
  size_t ndigits;

  assert (base >= 2);
  assert (base <= 36);

  un = GMP_ABS (u->_mp_size);
  if (un == 0)
    return 1;

  up = u->_mp_d;

  bits = (un - 1) * GMP_LIMB_BITS + mpn_limb_size_in_base_2 (up[un-1]);
  switch (base)
    {
    case 2:
      return bits;
    case 4:
      return (bits + 1) / 2;
    case 8:
      return (bits + 2) / 3;
    case 16:
      return (bits + 3) / 4;
    case 32:
      return (bits + 4) / 5;
    }

  tp = gmp_xalloc_limbs (un);
  mpn_copyi (tp, up, un);
  mpn_div_qr_1_invert (&bi, base);

  ndigits = 0;
  do
    {
      ndigits++;
      mpn_div_qr_1_preinv (tp, tp, un, &bi);
      un -= (tp[un-1] == 0);
    }
  while (un > 0);

  gmp_free (tp);
  return ndigits;
}

char *
mpz_get_str (char *sp, int base, const mpz_t u)
{
  unsigned bits;
  const char *digits;
  mp_size_t un;
  size_t i, sn;

  if (base >= 0)
    {
      digits = "0123456789abcdefghijklmnopqrstuvwxyz";
    }
  else
    {
      base = -base;
      digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
  if (base <= 1)
    base = 10;
  if (base > 36)
    return NULL;

  sn = 1 + mpz_sizeinbase (u, base);
  if (!sp)
    sp = (char *) gmp_xalloc (1 + sn);

  un = GMP_ABS (u->_mp_size);

  if (un == 0)
    {
      sp[0] = '0';
      sp[1] = '\0';
      return sp;
    }

  i = 0;

  if (u->_mp_size < 0)
    sp[i++] = '-';

  bits = mpn_base_power_of_two_p (base);

  if (bits)
    sn = i + mpn_get_str_bits ((unsigned char *) sp + i, bits, u->_mp_d, un);
  else
    {
      struct mpn_base_info info;
      mp_ptr tp;

      mpn_get_base_info (&info, base);
      tp = gmp_xalloc_limbs (un);
      mpn_copyi (tp, u->_mp_d, un);

      sn = i + mpn_get_str_other ((unsigned char *) sp + i, base, &info, tp, un);
      gmp_free (tp);
    }

  for (; i < sn; i++)
    sp[i] = digits[(unsigned char) sp[i]];

  sp[sn] = '\0';
  return sp;
}

int
mpz_set_str (mpz_t r, const char *sp, int base)
{
  unsigned bits;
  mp_size_t rn, alloc;
  mp_ptr rp;
  size_t sn;
  int sign;
  unsigned char *dp;

  assert (base == 0 || (base >= 2 && base <= 36));

  while (isspace( (unsigned char) *sp))
    sp++;

  sign = (*sp == '-');
  sp += sign;

  if (base == 0)
    {
      if (*sp == '0')
{
  sp++;
  if (*sp == 'x' || *sp == 'X')
    {
      base = 16;
      sp++;
    }
  else if (*sp == 'b' || *sp == 'B')
    {
      base = 2;
      sp++;
    }
  else
    base = 8;
}
      else
base = 10;
    }

  sn = strlen (sp);
  dp = (unsigned char *) gmp_xalloc (sn + (sn == 0));

  for (sn = 0; *sp; sp++)
    {
      unsigned digit;

      if (isspace ((unsigned char) *sp))
continue;
      if (*sp >= '0' && *sp <= '9')
digit = *sp - '0';
      else if (*sp >= 'a' && *sp <= 'z')
digit = *sp - 'a' + 10;
      else if (*sp >= 'A' && *sp <= 'Z')
digit = *sp - 'A' + 10;
      else
digit = base;

      if (digit >= base)
{
  gmp_free (dp);
  r->_mp_size = 0;
  return -1;
}

      dp[sn++] = digit;
    }

  bits = mpn_base_power_of_two_p (base);

  if (bits > 0)
    {
      alloc = (sn * bits + GMP_LIMB_BITS - 1) / GMP_LIMB_BITS;
      rp = MPZ_REALLOC (r, alloc);
      rn = mpn_set_str_bits (rp, dp, sn, bits);
    }
  else
    {
      struct mpn_base_info info;
      mpn_get_base_info (&info, base);
      alloc = (sn + info.exp - 1) / info.exp;
      rp = MPZ_REALLOC (r, alloc);
      rn = mpn_set_str_other (rp, dp, sn, base, &info);
    }
  assert (rn <= alloc);
  gmp_free (dp);

  r->_mp_size = sign ? - rn : rn;

  return 0;
}

int
mpz_init_set_str (mpz_t r, const char *sp, int base)
{
  mpz_init (r);
  return mpz_set_str (r, sp, base);
}

size_t
mpz_out_str (FILE *stream, int base, const mpz_t x)
{
  char *str;
  size_t len;

  str = mpz_get_str (NULL, base, x);
  len = strlen (str);
  len = fwrite (str, 1, len, stream);
  gmp_free (str);
  return len;
}

static void
mpz_div_q_2exp (mpz_t q, const mpz_t u, mp_bitcnt_t bit_index,
    enum mpz_div_round_mode mode)
{
  mp_size_t un, qn;
  mp_size_t limb_cnt;
  mp_ptr qp;
  int adjust;

  un = u->_mp_size;
  if (un == 0)
    {
      q->_mp_size = 0;
      return;
    }
  limb_cnt = bit_index / GMP_LIMB_BITS;
  qn = GMP_ABS (un) - limb_cnt;
  bit_index %= GMP_LIMB_BITS;

  if (mode == ((un > 0) ? GMP_DIV_CEIL : GMP_DIV_FLOOR)) /* un != 0 here. */
    /* Note: Below, the final indexing at limb_cnt is valid because at
       that point we have qn > 0. */
    adjust = (qn <= 0
        || !mpn_zero_p (u->_mp_d, limb_cnt)
        || (u->_mp_d[limb_cnt]
      & (((mp_limb_t) 1 << bit_index) - 1)));
  else
    adjust = 0;

  if (qn <= 0)
    qn = 0;

  else
    {
      qp = MPZ_REALLOC (q, qn);

      if (bit_index != 0)
  {
    mpn_rshift (qp, u->_mp_d + limb_cnt, qn, bit_index);
    qn -= qp[qn - 1] == 0;
  }
      else
  {
    mpn_copyi (qp, u->_mp_d + limb_cnt, qn);
  }
    }

  q->_mp_size = qn;

  if (adjust)
    mpz_add_ui (q, q, 1);
  if (un < 0)
    mpz_neg (q, q);
}

void
mpz_cdiv_qr (mpz_t q, mpz_t r, const mpz_t n, const mpz_t d)
{
  mpz_div_qr (q, r, n, d, GMP_DIV_CEIL);
}
void
mpz_fdiv_q_2exp (mpz_t r, const mpz_t u, mp_bitcnt_t cnt)
{
  mpz_div_q_2exp (r, u, cnt, GMP_DIV_FLOOR);
}
int miller_rabin(mpz_t n);

/********** ADD YOUR CODE BELOW ***********/
#include <iostream>
#include <map>
#include <array>
#include <cstdlib>
struct stringcomp {
    bool operator()(const std::string& a, const std::string& b) const {
      return std::atol(a.c_str()) < std::atol(b.c_str());
    }
};
std::map<std::string, int, stringcomp> ipf;
int p[168];

void initPrimes()
{
  p[0]=2;
  p[1]=3;
  p[2] = 5;
  p[3] = 7;
  p[4] = 11;
  p[5] = 13;
  p[6] = 17;
  p[7] = 19;
  p[8] = 23;
  p[9] = 29;
  p[10] = 31;
  p[11] = 37;
  p[12] = 41;
  p[13] = 43;
  p[14] = 47;
  p[15] = 53;
  p[16] = 59;
  p[17] = 61;
  p[18] = 67;
  p[19] = 71;
  p[20] = 73;
  p[21] = 79;
  p[22] = 83;
  p[23] = 89;
  p[24] = 97;
  p[25] = 101;
  p[26] = 103;
  p[27] = 107;
  p[28] = 109;
  p[29] = 113;
  p[30] = 127;
  p[31] = 131;
  p[32] = 137;
  p[33] = 139;
  p[34] = 149;
  p[35] = 151;
  p[36] = 157;
  p[37] = 163;
  p[38] = 167;
  p[39] = 173;
  p[40] = 179;
  p[41] = 181;
  p[42] = 191;
  p[43] = 193;
  p[44] = 197;
  p[45] = 199;
  p[46] = 211;
  p[47] = 223;
  p[48] = 227;
  p[49] = 229;
  p[50] = 233;
  p[51] = 239;
  p[52] = 241;
  p[53] = 251;
  p[54] = 257;
  p[55] = 263;
  p[56] = 269;
  p[57] = 271;
  p[58] = 277;
  p[59] = 281;
  p[60] = 283;
  p[61] = 293;
  p[62] = 307;
  p[63] = 311;
  p[64] = 313;
  p[65] = 317;
  p[66] = 331;
  p[67] = 337;
  p[68] = 347;
  p[69] = 349;
  p[70] = 353;
  p[71] = 359;
  p[72] = 367;
  p[73] = 373;
  p[74] = 379;
  p[75] = 383;
  p[76] = 389;
  p[77] = 397;
  p[78] = 401;
  p[79] = 409;
  p[80] = 419;
  p[81] = 421;
  p[82] = 431;
  p[83] = 433;
  p[84] = 439;
  p[85] = 443;
  p[86] = 449;
  p[87] = 457;
  p[88] = 461;
  p[89] = 463;
  p[90] = 467;
  p[91] = 479;
  p[92] = 487;
  p[93] = 491;
  p[94] = 499;
  p[95] = 503;
  p[96] = 509;
  p[97] = 521;
  p[98] = 523;
  p[99] = 541;
  p[100] = 547;
  p[101] = 557;
  p[102] = 563;
  p[103] = 569;
  p[104] = 571;
  p[105] = 577;
  p[106] = 587;
  p[107] = 593;
  p[108] = 599;
  p[109] = 601;
  p[110] = 607;
  p[111] = 613;
  p[112] = 617;
  p[113] = 619;
  p[114] = 631;
  p[115] = 641;
  p[116] = 643;
  p[117] = 647;
  p[118] = 653;
  p[119] = 659;
  p[120] = 661;
  p[121] = 673;
  p[122] = 677;
  p[123] = 683;
  p[124] = 691;
  p[125] = 701;
  p[126] = 709;
  p[127] = 719;
  p[128] = 727;
  p[129] = 733;
  p[130] = 739;
  p[131] = 743;
  p[132] = 751;
  p[133] = 757;
  p[134] = 761;
  p[135] = 769;
  p[136] = 773;
  p[137] = 787;
  p[138] = 797;
  p[139] = 809;
  p[140] = 811;
  p[141] = 821;
  p[142] = 823;
  p[143] = 827;
  p[144] = 829;
  p[145] = 839;
  p[146] = 853;
  p[147] = 857;
  p[148] = 859;
  p[149] = 863;
  p[150] = 877;
  p[151] = 881;
  p[152] = 883;
  p[153] = 887;
  p[154] = 907;
  p[155] = 911;
  p[156] = 919;
  p[157] = 929;
  p[158] = 937;
  p[159] = 941;
  p[160] = 947;
  p[161] = 953;
  p[162] = 967;
  p[163] = 971;
  p[164] = 977;
  p[165] = 983;
  p[166] = 991;
  p[167] = 997;
}
void factor_more(mpz_t n)
{
  if(1==miller_rabin(n))
  {
    ipf[mpz_get_str(NULL, 10, n)]++;
    return;
  }
  std::string input;
  input = mpz_get_str(NULL, 10, n);
  for(int i=0; i<168; i++) 
  {
    mpz_t divisor, quotient, remain;
    mpz_init(quotient);
    mpz_init(remain);
    mpz_init_set_ui(divisor, p[i]);
    mpz_t square;
    mpz_init(square);
    mpz_mul(square, divisor, divisor);
    if(mpz_cmp(square, n) > 0)
    {
      ipf[input.c_str()]++;
      return;
    }
    mpz_cdiv_qr(quotient, remain, n, divisor);
    if(mpz_cmp_ui(remain, 0) == 0)
    {  
      std::string add;
      add = mpz_get_str(NULL, 10, divisor);
      ipf[add.c_str()]++;
      factor_more(quotient);
      return;
    }
  }
  if(mpz_cmp_ui(n, 1) == 0) {
    return;
  }
  ipf[input.c_str()]++;
}
void gcd(mpz_t result, mpz_t a, mpz_t b)
{
  mpz_t bcopy,zero,remainder;
  mpz_init_set(bcopy,b);
  mpz_init_set_si(zero,0);
  mpz_init(remainder);
  while(mpz_cmp(bcopy,zero) != 0)
  {
    mpz_mod(remainder,a,bcopy);
    mpz_set(a,bcopy);
    mpz_set(bcopy,remainder);
  }
  mpz_set(result,a);
  mpz_clear(bcopy);
  mpz_clear(zero);
  mpz_clear(remainder);
}
void find_factor(mpz_t result, mpz_t n)
{
  mpz_t number,x,factor,one,z,y,tmp;
  mpz_init_set_ui(x, 2);
  mpz_init_set_ui(y, 2);
  mpz_init(tmp);
  mpz_init_set_ui(z,1);
  mpz_init_set(number,n);
  mpz_init_set_ui(factor, 1);
  mpz_init_set_ui(one, 1);
  int power = 1;
  int lam = 1;
  while(mpz_cmp(factor, one) == 0)
  {
    for(int i=0; i<24; ++i)
    {
      if(power == lam)
      {
        mpz_set(x,y);
        power *= 2;
        lam = 0;
      }
      mpz_mul(y,y,y);
      mpz_add_ui(y,y,1);
      mpz_mod(y,y,number);
      lam++;
      mpz_sub(tmp,y,x);
      mpz_abs(tmp,tmp);
      mpz_mul(factor,factor,tmp);
    }
    gcd(factor,factor,number);
  }
  mpz_divexact(result,number,factor);
  factor_more(factor);
  mpz_clear(number);
  mpz_clear(x);
  mpz_clear(factor);
  mpz_clear(one);  
}

#define COMPOSITE        0
#define PROBABLE_PRIME   1

void modular_pow(mpz_t result, mpz_t base, mpz_t expo, mpz_t mod)
{ 
  if(mpz_cmp_ui(mod, 1) == 0)
  {
    mpz_set_ui(result, 0);
    return;
  }
  mpz_set_ui(result,1);
  mpz_t t;
  mpz_init_set_ui(t,0);
  mpz_mod(base,base,mod);
  while(mpz_cmp_ui(expo,0)>0)
  { 
    mpz_mod_ui(t,expo,2);
    if(mpz_cmp_ui(t,1) == 0)
    {
      mpz_mul(result,result,base);
      mpz_mod(result,result,mod);
    }
    mpz_fdiv_q_2exp(expo,expo,1);
    mpz_mul(base,base,base);
    mpz_mod(base,base,mod);
  }
}

int miller_rabin_pass(mpz_t a, mpz_t n)
{
  int i, s=0, result;
  mpz_t a_to_power, d, n_minus_one, two;
  mpz_init(n_minus_one);
  mpz_sub_ui(n_minus_one, n, 1);
  mpz_init_set(d, n_minus_one);
  mpz_init_set_ui(two, 2);
  while (mpz_even_p(d))
  {
    mpz_fdiv_q_2exp(d, d, 1);
    s++;
  }
  mpz_init(a_to_power);
  modular_pow(a_to_power, a, d, n);

  if(mpz_cmp_ui(a_to_power, 1) == 0)
  {
    result=PROBABLE_PRIME; goto exit;
  }
  for(i=0; i < s-1; i++)
  {
    if(mpz_cmp(a_to_power, n_minus_one) == 0)
    {
      result=PROBABLE_PRIME; goto exit;
    }
    modular_pow(a_to_power, a_to_power, two, n);
  }
  if(mpz_cmp(a_to_power, n_minus_one) == 0)
  {
    result=PROBABLE_PRIME; goto exit;
  }
  result = COMPOSITE;
exit:
  mpz_clear(a_to_power);
  mpz_clear(d);
  mpz_clear(n_minus_one);
  return result;
}

long lrand()
{
  if(sizeof(int) < sizeof(long))
    return (static_cast<long>(rand()) << (sizeof(int) * 8)) |
     rand();
  return rand();
}
int miller_rabin(mpz_t n)
{
  mpz_t a, firstL;
  int repeat;
  unsigned long r;
  mpz_init(a);
  mpz_init(firstL);

  for(repeat=0; repeat<20; ++repeat)
  {
    mpz_set_ui(a, 0);
    while(mpz_cmp_ui(a, 0) == 0)
    { 
      r = lrand();
      mpz_set_ui(a, r);
      mpz_mod(a, a, n);
      mpz_mul(a, a, a);
      mpz_mod(a, a, n);
    }

    if (miller_rabin_pass(a,n) == COMPOSITE) {
        return COMPOSITE;
    }
  }

  return PROBABLE_PRIME;
}
int main()
{
  mpz_t new_number,n;
  mpz_init(new_number);
  initPrimes();
  char input[32];
  while(scanf("%31s",input)&&strncmp(input,"0",1)!=0) { 
    mpz_init_set_str(n,input,10);
    if(mpz_odd_p(n)==0)
    {
      ipf["2"]++;
      mpz_divexact_ui(n,n,2);
      if(mpz_odd_p(n)==0)
      {
        ipf["2"]++;
        mpz_divexact_ui(n,n,2);
      }
    }
    if(1 == miller_rabin(n))
    {
      ipf[mpz_get_str(NULL, 10, n)]++;
      goto print;
    }
    if(mpz_cmp_ui(n, 994009) < 1)
    {
      factor_more(n);
      goto print;
    } 
    
    while(1)
    {
      find_factor(new_number,n);
      if(mpz_cmp_ui(new_number,1) == 0)
      {
        break;
      }
      mpz_set(n,new_number);
    }
  print:
    for(auto it=ipf.cbegin();it!=ipf.cend(); ++it)
    {
      std::cout<<it->first<<"^"<< it->second<< " ";
    }
    printf("\n");
    ipf.clear();
  }
}